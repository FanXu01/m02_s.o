#/bin/bash
        echo
        echo "Selecciona l'opcio"
        echo
        echo "1. Inseresix números a un fitxer"
        echo
        echo "2. Esborra numeros del fitxer"
        echo
        echo "3. Quin numero vols trobar al fitxer"
        echo
        echo "4. Visualitza el contingut del fitxer"
        echo
        echo "5. Per sortir"
        echo
        echo "Introdueix l'opció"
        read OPCION

        case $OPCION
        in

                1)
                 echo "insereix el nom del fitxer"
                 read fitxer
                 echo "He creat el fitxer ara insereix el numero"
                 read numero
                 touch $numero > $fitxer
                 ;;

                2)
                 echo "Borra numeros del $fitxer"
                 touch $fitxer
                 ;;

                3)
                 echo "Introdueix el numero que vols cercar"
                 read numero
                 grep "$numero" $fitxer
                 ;;

                4)
                 echo "Visualitza el contingut del fitxer"
                 ;;

                5)
                 echo "adeu"
                 exit
        esac

