#"Amb bash, que hem de fer per rebre dades per consola a un usuari?"
# Amb el read
#"Amb bash, com recorrem tots els fitxers amb extensió .jpg d'un directori?"
# 
#"Amb bash, com comprovem si una variable té valor? "
# Amb el -z
#"Quina comanda fem servir per renombrar un fitxer?"
# Amb el mv podem renombrar un fitxer

#/bin/bash
#Script per renombrar els noms dels fitxers segons el tipus d'extensio

DAY=$(date +%F)
Directori=$"/ism5677930/Pictures/*.jpg"

echo "Quina extensió de fitxer volem considerar?"
        read -p "Introdueix l'extencio: " EXTENSION
echo "Quin es el prefix?"
        read -p "Introdueix el prefix: " PREFIJO
for NAME in *.EXTENSION
do
        echo "Renombra $NAME to ${DAY-${NAME}"
        if [ -z $PREFIJO ];
        then
                mv $NAME ${DAY}-${NAME}
        else
                mv $NAME $PREFIJO-${NAME}
        fi
done

