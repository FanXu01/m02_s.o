# E3. Exercici 2. JournalD

## Introducció

## Continguts

Amb els fiters i directoris de la carpeta exercici responeu les segûents preguntes:

Indiqueu en cada pregunta l'ordre i també el resultat obtingut (podeu copiar del terminal)

## Entrega

1. **Analitzeu els logs de l'última arrencada del vostre sistema operatiu i indiqueu:**
   - Ordre per mostrar el log:
   
   [ism5677930@a12 ~]$ journalctl

-- Logs begin at Tue 2020-09-15 12:11:12 CEST, end at Tue 2021-05-04 09:55:52 C>
Sep 15 12:11:12 localhost.localdomain kernel: microcode: microcode updated earl>
Sep 15 12:11:12 localhost.localdomain kernel: Linux version 5.8.8-200.fc32.x86_>
Sep 15 12:11:12 localhost.localdomain kernel: Command line: BOOT_IMAGE=(hd0,msd>
Sep 15 12:11:12 localhost.localdomain kernel: x86/fpu: Supporting XSAVE feature>
Sep 15 12:11:12 localhost.localdomain kernel: x86/fpu: Supporting XSAVE feature>
Sep 15 12:11:12 localhost.localdomain kernel: x86/fpu: Enabled xstate features >
Sep 15 12:11:12 localhost.localdomain kernel: BIOS-provided physical RAM map:
Sep 15 12:11:12 localhost.localdomain kernel: BIOS-e820: [mem 0x000000000000000>
Sep 15 12:11:12 localhost.localdomain kernel: BIOS-e820: [mem 0x000000000009d80>
Sep 15 12:11:12 localhost.localdomain kernel: BIOS-e820: [mem 0x00000000000e000>
Sep 15 12:11:12 localhost.localdomain kernel: BIOS-e820: [mem 0x000000000010000>
Sep 15 12:11:12 localhost.localdomain kernel: BIOS-e820: [mem 0x00000000c67c800>
Sep 15 12:11:12 localhost.localdomain kernel: BIOS-e820: [mem 0x00000000c67cf00>
Sep 15 12:11:12 localhost.localdomain kernel: BIOS-e820: [mem 0x00000000c77fd00>
Sep 15 12:11:12 localhost.localdomain kernel: BIOS-e820: [mem 0x00000000c7d6f00>
Sep 15 12:11:12 localhost.localdomain kernel: BIOS-e820: [mem 0x00000000d9f1f00>
Sep 15 12:11:12 localhost.localdomain kernel: BIOS-e820: [mem 0x00000000da13300>
Sep 15 12:11:12 localhost.localdomain kernel: BIOS-e820: [mem 0x00000000da21900>
Sep 15 12:11:12 localhost.localdomain kernel: BIOS-e820: [mem 0x00000000dafff00>
Sep 15 12:11:12 localhost.localdomain kernel: BIOS-e820: [mem 0x00000000db80000>
Sep 15 12:11:12 localhost.localdomain kernel: BIOS-e820: [mem 0x00000000f800000>
Sep 15 12:11:12 localhost.localdomain kernel: BIOS-e820: [mem 0x00000000fec0000>


   - Ordre per veure només els errors amb prioritat d'error des de l'última arrencada:
   journalctl -p err -b
   - Ordre per veure només els errors amb prioritat d'emergència des de l'última arrencada:
   journalctl -p emerg -b
[ism5677930@a12 ~]$ journalctl -p err -b
-- Logs begin at Tue 2020-09-15 12:11:12 CEST, end at Tue 2021-05-04 09:59:16 C>
May 04 08:04:11 localhost.localdomain smartd[555]: Device: /dev/sda [SAT], 16 C>
May 04 08:04:11 localhost.localdomain smartd[555]: Device: /dev/sda [SAT], 16 O>
May 04 08:05:10 a12 krb5_child[1303][1303]: No credentials cache found (filenam>
May 04 08:05:10 a12 gdm-password][1268]: gkr-pam: unable to locate daemon contr>
May 04 08:05:11 a12 systemd[1307]: (pam_mount.c:173): conv->conv(...): Conversa>
May 04 08:05:11 a12 systemd[1307]: (pam_mount.c:476): warning: could not obtain>
May 04 08:05:12 a12 systemd[1307]: (pam_mount.c:173): conv->conv(...): Conversa>
May 04 08:05:12 a12 systemd[1307]: (pam_mount.c:476): warning: could not obtain>
May 04 08:05:12 a12 gdm-password][1268]: (mount.c:68): Messages from underlying>
May 04 08:05:12 a12 gdm-password][1268]: (mount.c:72): mount.nfs4: access denie>
May 04 08:05:12 a12 gdm-password][1268]: (pam_mount.c:522): mount of /users/inf>
May 04 08:34:11 a12 smartd[555]: Device: /dev/sda [SAT], 16 Currently unreadabl>
May 04 08:34:11 a12 smartd[555]: Device: /dev/sda [SAT], 16 Offline uncorrectab>
May 04 09:04:11 a12 smartd[555]: Device: /dev/sda [SAT], 16 Currently unreadabl>
May 04 09:04:11 a12 smartd[555]: Device: /dev/sda [SAT], 16 Offline uncorrectab>
May 04 09:34:11 a12 smartd[555]: Device: /dev/sda [SAT], 16 Currently unreadabl>
May 04 09:34:11 a12 smartd[555]: Device: /dev/sda [SAT], 16 Offline uncorrectab>
lines 1-18/18 (END)

   
[ism5677930@a12 ~]$ journalctl -p emerg -b
-- Logs begin at Tue 2020-09-15 12:11:12 CEST, end at Tue 2021-05-04 09:59:16 C>
-- No entries --

   - Ordre per veure només els errors amb prioritat d'alerta des de l'última arrencada:
   journalctl -p alert -b
   
[ism5677930@a12 ~]$ journalctl -p alert -b

-- Logs begin at Tue 2020-09-15 12:11:12 CEST, end at Tue 2021-05-04 09:59:16 C>
-- No entries --
lines 1-2/2 (END)


2. **Com podem veure el log del nostre mini-servidor web que arrenquem amb el systemd?**
   - Ordre: journalctl -u myweb.service -f
   - Sortida de l'ordre anterior:

[ism5677930@a12 ~]$ journalctl -u myweb.service -f

-- Logs begin at Tue 2020-09-15 12:11:12 CEST. --
Apr 27 10:12:37 a12 systemd[1]: Started Servidor WEB.
Apr 27 13:51:37 a12 systemd[1]: Stopping Servidor WEB...
Apr 27 13:51:37 a12 systemd[1]: myweb.service: Succeeded.
Apr 27 13:51:37 a12 systemd[1]: Stopped Servidor WEB.
Apr 27 13:51:37 a12 systemd[1]: myweb.service: Consumed 1.628s CPU time.

3. **Com podem veure els accessos que es fan al nostre servidor en temps real des del log de journal? **
   - Ordre: cat /var/log/httpd/access_log

4. **Feu un cron que comprovi cada minut si el servei sshd està corrent o no al nostre sistema operatiu.**
   - Ordre i/o fitxers per a dur a terme el cron:
   
   */1 * * * * systemclt is -active sshd
   
5. **Feu ara que aquest cron miri si el servei sshd està corrent i el pari i enviï un missatge d'error de nivell emergència al journal..**
   - Ordre i/o fitxers per a dur a terme el cron:

[ism5677930@a12 ~]$ ***** /us/bin/bash /home/users/inf/hism1/ism5677930/checkers.sh 

   
   #!/bin/bash

systemctl is-active sshd

if [ $? == 0 ]; then
        systemctl stop sshd
        echo "Error de nivell emergencia" | systemd-cat -p emerg
else
        echo "Tot esta bé" | systemd-cat -p info
fi

   - Proveu d'iniciar i aturar aquest servei i mostreu l'ordre amb la que podem monitoritzar si està o no funcionant (mostra missatges al journal)
