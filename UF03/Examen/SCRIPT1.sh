#!/bin/bash

systemctl is-active ngnix.service

if [ $? == 0 ]; then
        systemctl stop nginx.service
        echo "Error de nivell emergencia" | systemd-cat -p emerg
else
        echo "Tot esta bé" | systemd-cat -p info
fi
