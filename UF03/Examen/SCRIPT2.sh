#!/bin/bash

systemctl is-active ngnix.service

if [ $? == 1 ]; then
        echo "Servei nginx aturat procedeixo a iniciar-ho"
        systemctl start nginx.service
else
        echo "Servei nginx esta actiu"
fi
