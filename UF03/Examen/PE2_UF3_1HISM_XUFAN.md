## Autor:          PE2_UF3_Grup_Cognom_alumne.md
## Date:           28/05/21
## Description:    Prova M03 - UF03 SystemD, Journal, Firewall, Quotes
## Entrega Prova escrita UF3

En finalitzar l'exercici, el fitxer l'heu d'annexar al Moddle, **anomena'l amb el següent format: PE2_UF3_Grup_Cognom_nom.md**
També, recorda pujar-ho al GIT, a la carpeta UF3/prova-escrita/

## Enunciat

Estem encarregats d'administrar el sistema operatiu linux d'un servidor a la nostra empresa. Se'ns demana que fem diverses coses per tal d'obtenir un servidor amb les funcionalitats que es requereixen:

- Cal un servidor web.
- Caldrà establir un tallafocs per tal que no ens accedeixin a ports que no siguin públics.
- Caldrà un accés de ssh per a la gestió remota d'aquest servidor, però volen que només sigui accessible en horari d'oficina (tot i que la màquina estarà en funcionament tot el dia)

1. [2 punts] **Cal que aquesta màquina vostra tingui accés per ssh i que estigui en hora. Per això comprovarem els serveis:**

    - [0,25 punts] Ordre per activar el servei de ssh en iniciar el sistema operatiu:
    
    	[ism5677930@a12 ~]$ sudo systemctl start sshd
    
    - [0,25 punts] Ordre per arrencar ara el servei de ssh:
    
	[ism5677930@a12 ~]$ sudo systemctl enable sshd
    
    - [0,25 punts] Odre i sortida per veure l'estat del servei ssh:
    
    [ism5677930@a12 ~]$ sudo systemctl status sshd
	● sshd.service - OpenSSH server daemon
     Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset: disabled)
     Active: active (running) since Fri 2021-05-28 09:16:26 CEST; 1h 11min ago
       Docs: man:sshd(8)
             man:sshd_config(5)
   Main PID: 730 (sshd)
      Tasks: 1 (limit: 9366)
     Memory: 2.1M
        CPU: 22ms
     CGroup: /system.slice/sshd.service
             └─730 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups

May 28 09:16:26 localhost.localdomain sshd[730]: Server listening on 0.0.0.0 port 22.
May 28 09:16:26 localhost.localdomain sshd[730]: Server listening on :: port 22.
May 28 09:16:24 localhost.localdomain systemd[1]: Starting OpenSSH server daemon...
May 28 09:16:26 localhost.localdomain systemd[1]: Started OpenSSH server daemon.

    
    - [0,25 punts] Ordre per activar el servei d'hora en iniciar el sistema operatiu:
  
    [ism5677930@a12 ~]$ sudo systemctl start ntpd.service 

    
    - [0,25 punts] Ordre per arrencar ara el servei d'hora:
    
    [ism5677930@a12 ~]$ sudo systemctl enable ntpd.service 
Created symlink /etc/systemd/system/multi-user.target.wants/ntpd.service → /usr/lib/systemd/system/ntpd.service.

    - [0,25 punts] Ordre i sortida per veure l'estat del servei d'hora:
    
    [ism5677930@a12 ~]$ sudo systemctl status ntpd.service 
● ntpd.service - Network Time Service
     Loaded: loaded (/usr/lib/systemd/system/ntpd.service; enabled; vendor preset: disabled)
     Active: active (running) since Fri 2021-05-28 11:04:58 CEST; 16s ago
   Main PID: 22226 (ntpd)
      Tasks: 2 (limit: 9366)
     Memory: 1.0M
        CPU: 18ms
     CGroup: /system.slice/ntpd.service
             └─22226 /usr/sbin/ntpd -u ntp:ntp -g

May 28 11:04:58 a12 ntpd[22226]: Listen normally on 3 enp2s0 10.200.246.212:123
May 28 11:04:58 a12 ntpd[22226]: Listen normally on 4 lo [::1]:123
May 28 11:04:58 a12 ntpd[22226]: Listen normally on 5 enp2s0 [fe80::36b5:43a3:8adb:3b18%2]:123
May 28 11:04:58 a12 ntpd[22226]: Listening on routing socket on fd #22 for interface updates
May 28 11:04:58 a12 ntpd[22226]: kernel reports TIME_ERROR: 0x41: Clock Unsynchronized
May 28 11:04:58 a12 ntpd[22226]: kernel reports TIME_ERROR: 0x41: Clock Unsynchronized
May 28 11:04:58 a12 systemd[1]: Started Network Time Service.
May 28 11:04:59 a12 ntpd[22226]: Soliciting pool server 147.156.7.18
May 28 11:05:00 a12 ntpd[22226]: Soliciting pool server 178.32.88.247
May 28 11:05:01 a12 ntpd[22226]: Soliciting pool server 2a01:9e40::abcd

    - [0,5 punts] Ordre per veure tots els seveis que es troben arrencats actualment:
    
    [ism5677930@a12 ~]$ sudo systemctl list-unit-files --type service --all
    
UNIT FILE                                     STATE           VENDOR PRESET
abrt-journal-core.service                     enabled         enabled 
abrt-oops.service                             enabled         enabled 
abrt-pstoreoops.service                       disabled        disabled 

    

2. [2 punts] **Instal.leu ara el servidor web nginx. Aquest servidor ens permetrà tenir un servei en producció:**

    - [0,50 punts] Ordre per instal.lar el servidor web nginx al vostre sistema operatiu:
    
  	sudo dnf install nginx

    - [0,50 punts] Ordre per activar el servei nginx en iniciar el sistema operatiu:
    
	[ism5677930@a12 ~]$ systemctl start nginx.service 


    - [0,50 punts] Ordre per arrencar ara el servei nginx:

	[ism5677930@a12 ~]$ systemctl enable nginx.service 
			    Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.

    - [0,50 punts] Ordre i sortida per a veure els últims 10 missatges de log del servei nginx:
	    
	[ism5677930@a12 ~]$ journalctl -u nginx.service -f
	-- Logs begin at Tue 2020-09-15 12:11:12 CEST. --
	May 28 10:17:06 a12 systemd[1]: Starting The nginx HTTP and reverse proxy server...
	May 28 10:17:07 a12 nginx[4698]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
	May 28 10:17:07 a12 nginx[4698]: nginx: configuration file /etc/nginx/nginx.conf test is successful
	May 28 10:17:07 a12 systemd[1]: Started The nginx HTTP and reverse proxy server.


3. [2 punts] **Ara cal assegurar el servei ssh i el web com a els únics als que permetem accés:**

   - [0,25 punts] Ordre per activar el servei de tallafocs en iniciar el sistema operatiu:
   
   	[ism5677930@a12 ~]$ sudo systemctl start firewalld
	[ism5677930@a12 ~]$ echo $?
		            0

   
   - [0,25 punts] Ordre per arrencar ara el servei de tallafocs:
   
  	 sudo systemctl enable firewalld
  	 Per verificar: sudo firewall-cmd --state
  	 
   - [0,25 punts] Ordre i sortida on es vegi els ports que el tallafocs està filtrant actualment i la zona activa:
   
   	[ism5677930@a12 ~]$ firewall-cmd --get-active-zones
FedoraWorkstation
  interfaces: enp2s0
libvirt
  interfaces: virbr0

	[ism5677930@a12 ~]$ sudo firewall-cmd --list-all
FedoraWorkstation (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp2s0
  sources: 
  services: dhcpv6-client samba-client ssh
  ports: 1025-65535/udp 1025-65535/tcp
  protocols: 
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 

   - [0,5 punts] Ordre(s) per tal d'aconseguir que només siguin accessibles els ports 22 i 80 del servidor a partir de la informació mostrada en l'ordre anterior:
   
   [ism5677930@a12 ~]$ sudo firewall-cmd --zone=public --add-port=22/tcp
[sudo] password for ism5677930: 
success
[ism5677930@a12 ~]$ sudo firewall-cmd --zone=public --add-port=80/tcp
success

   - [0,5 punts] Ordre i sortida on es vegi els ports que el tallafocs està filtrant actualment i la zona activa:
   
[ism5677930@a12 ~]$ sudo firewall-cmd --zone=public --list-ports
22/tcp 80/tcp

[ism5677930@a12 ~]$ firewall-cmd --get-active-zones
FedoraWorkstation
  interfaces: enp2s0
libvirt
  interfaces: virbr0

  
   - [0,25 punts] Feu que aquesta configuració sigui permanent. Indiqueu la/les ordre(s) per a fer-ho:
   
[ism5677930@a12 ~]$ sudo firewall-cmd --zone=public --permanent --add-port=22/tcp
[sudo] password for ism5677930: 
success


Per fer-ho permanent només tenim que afegir un --permanent després de la zona.

4. [2 punts] **Feu ara dos scripts que comprovin l'estat del servei nginx. Un l'haurà d'arrencar si no ho està i l'altre l'haurà d'aturar si està arrencat**

    - [0,5 punts] Script que comprova si està arrencat i l'atura
    
#!/bin/bash

systemctl is-active ngnix.service

if [ $? == 0 ]; then
        systemctl stop nginx.service
        echo "Error de nivell emergencia" | systemd-cat -p emerg
else
        echo "Tot esta bé" | systemd-cat -p info
fi

    - [0,5 punts] Script que comprova si està aturat i l'arrenca
    
#!/bin/bash

systemctl is-active ngnix.service

if [ $? == 1 ]; then
        echo "Servei nginx aturat procedeixo a iniciar-ho"
        systemctl start nginx.service 
else 
        echo "Servei nginx esta actiu"
fi
    
    - [0,5 punts] Què hauríem de canviar al nostre script per tal que enviés un missatge de prioritat 'info' al journal amb informació que ens digui si l'hem arrencat o si l'hem parat?
   
    	Posant aixo despres de echo | systemd-cat -p info
   
    - [0,5 punts] Com podem veure els missatges de prioritat 'info' del log?
   
   	Amb (journalctl -p err -b)

5. [2 punts]  **Una mica sobre quotes:** 
   - [0,25 punts] Per a que serveixen les quotes de disc? Serveixen per restringir espai al disc dur per a un usuari abans de que consumeixi massa espai en disc o una partició s’omplí.
   Les quotes de disc es poden configurar tant per a usuaris individuals com per a grups d’usuaris. Això permet gestionar l’espai assignat als fitxers específics de l’usuari (com ara el correu electrònic) per separat de l’espai assignat als projectes en què treballa un usuari (suposant que els projectes reben els seus propis grups).
A més, es poden establir quotes no només per controlar el nombre de blocs de disc consumits sinó per controlar el nombre d’inodes.

   - [0,50 punts] creem un disc virtual de 80Mb anomenat examen a la carpeta /home/
   
   sudo dd if=/dev/zero of=/home/examen/memoria bs=80mb count=1000
  
   - [0,25 punts] Al disc virtual que acabem de crear li donem format ext4
  
     sudo mkfs.ext4 /home/examen/memoria
  
   - [0,50 punts] Montem l'arxiu examen a la carpeta /mnt/usuari
  
      mkdir /mnt/usuari
   sudo mount -o loop /home/examen/memoria /mnt/usurai
  
   - [0,25 punts] Quin fitxer hem de modificar, per tal de que es munti el disc automàticament en reiniciar el sistema. 
   
   /etc/fstab
 el fitxer fstab
 
   - [0,25 punts] Que és un inode?

	Estructures de dades que contenen informació sobre fitxers en sistemes de fitxers UNIX, els inodes s’utilitzen per contenir informació relacionada amb el fitxer, això permet controlar el nombre de fitxers que es poden crear.
