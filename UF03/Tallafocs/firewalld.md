# Autor:          e3.4_Firewall_1HISM_XUFNA.md
# Date:           04/05/21
# Description:    E3.4 Treball de recerca sobre els Firewalls

## 1. Qué és un tallafocs?. 
El tallafocs es una part de un sistema operatiu que actua com fronted per sistemes de filtratge.

## 2. Funcions dels tallafocs?. 
Bloquejar l'accés no autoritzat, permetent al mateix temps comunicacions autoritzades.​ Els tallafocs. poden ser implementats en maquinari o programari, o en una combinació de tots dos.

## 3. Qué és el Firewalld?. 
Firewalld és una solució de gestió de tallafocs disponible per a moltes distribucions de Linux que actuen com un frontend per al sistema de filtratge de paquets iptables proporcionat pel nucli de Linux.


## 4. Qué és una zona en un firewall?. Quina és la zona d'un Firewall de més confiança?. i la de menys?
Meny confiança: drop, el nivell de confiança més baix. Totes les connexions entrants es redueixen sense resposta i només es poden fer connexions de sortida.
Més confiança: trusted confiï en totes les màquines de la xarxa. Les opcions disponibles més obertes i s'han d’utilitzar amb moderació.

## 5. Que defineixen les regles en un Firewall?. 
En firewalld, es poden designar regles permanents o immediates. Si s'afegeix o modifica una regla, per defecte es modifica el comportament del tallafoc actual. A la següent arrencada, es revertiran les regles antigues.

## 6. Com verifiquem que el servei de firewall està en execució?
sudo firewall-cmd --state

## 7. Com instal·lem i habilitem un tallafoc?
sudo yum install firewalld
sudo systemctl start firewalld
sudo systemctl enable firewalld
sudo reboot

## 8. Com explorem les zones disponibles d'un firewall?

firewall-cmd --get-active-zones


## 9. Com sabem quina zona del Firewall està seleccionada com a predeterminada? 
firewall-cmd --get-default-zone


## 10. Podem veure la configuració específica associada a la zona home?
sudo firewall-cmd --zone=home --list-all


## 11. Com sabem quines regles estan associades a la zona pública? 
sudo firewall-cmd --list-all


## 12. Que és una xarxa?. Com fem per indicar les interficies d'una xarxa?. Com activem una interficie de xarxa?  




## 13. Com indiquem que una regla sigui permanent? 
sudo firewall-cmd --zone=public --add-service=http --permanent
sudo firewall-cmd --reload



## 14. Com podem saber els serveis disponibles del tallafocs?
sudo firewall-cmd --get-services




## 15. Com podem canviar una regla?




## 16. Si volem saber més informació de cadascun dels serveis del firewall. Com ho podem fer?.
Podeu obtenir més informació sobre cadascun d’aquests serveis si mireu el .xmlfitxer associat dins del /usr/lib/firewalld/servicesdirectori. 

/usr/lib/firewalld/services/ssh.xml

<?xml version="1.0" encoding="utf-8"?>
<service>
  <short>SSH</short>
  <description>Secure Shell (SSH) is a protocol for logging into and executing commands on remote machines. It provides secure encrypted communications. If you plan on accessing your machine remotely via SSH over a firewalled interface, enable this option. You need the openssh-server package installed for this option to be useful.</description>
  <port protocol="tcp" port="22"/>
</service>

## 17. Quina es la manera mes senzilla de definir excepcions de tallafocs per a serveis ?




## 18. Com podem habilitar un servei per a una zona?. Com podem verificar que el servei es faci permanent (que el  servei encara estigui disponible després d'un reinici. )? 





## 19. Que és un port?. Com obrim un port per a una zona?. Com verifiquem que hagi anat bé?





## 20. Fes un dibuix on apareguins els següents elements: Linux, Kernel, Firewall, zones, Regles, serveis, terminal, xarxa, port, wifi, public, home, work, Internet. Annexa'l al markdown.
