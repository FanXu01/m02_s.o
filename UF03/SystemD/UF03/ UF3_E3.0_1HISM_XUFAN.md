# Autor:          UF3_E3.0_Grup_Cognom_alumne.md
# Date:           16/04/21
# Description:    E3. Exercici 0. Treball de recerca sobre el SystemD

## 1. Qué és systemD?. Qué és SysV?. 

Systemd és un conjunt de dimonis o daemons d'administració de sistema, biblioteques i eines dissenyats com una plataforma d'administració i configuració central per a interactuar amb el nucli del Sistema operatiu GNU/Linux.

System V, abreujat comunament SysV i rarament System 5, va ser una de les versions del sistema operatiu Unix.

## 2. En quin fitxer es troba la configuració general de systemd?

Es troban a:

/usr/lib/systemd/system: Aquí hi ha els que venen amb els paquets que intal.lem (.deb, .rpm,...)

/run/systemd/system: Aquí hi ha els que es creen en inicial el sistema. Són més prioritaris que els anterior.

/etc/systemd/system: Aquí és on com administrador podem afegir-hi les nostres configuracions de serveis, que seran més prioritaris que tots els anteriors.

## 3. Quina comanda fem servir per saber la versió actual de SystemD? 

```
ps -eaf | grep [s]ystemd
```

## 4. Que són les units en un systemD?. Fica 3 exemples.

Podem veure totes les unitats disponibles amb:

```plaintext
systemctl list-unit-files
```

I les que s'estan executant:

```plaintext
systemctl list-units
```

O les que han fallat en iniciar-se:

```plaintext
systemctl --failed
```

## 5. Quina informació trobem en els fitxers amb extensió .swap?

Informació temporal 

## 6. Que consultem amb la següent comanda?
```
   systemctl list-units --type service
```

## 7. Quins estats pot tenir una Unit?. Fes una descripció.

**Enabled:** El servicio está habilitado, se está usando o está disponible para su uso.

**Disabled:** El servicio está deshabilitado. Si lo necesitamos lo podemos habilitar sin mayor problema.

**Masked:** El servicio está completamente deshabilitado y no se puede iniciar de ningún modo sin previamente desenmascararlo.

**Static:** Servicios que únicamente se usarán en el caso que otro servicio o unidad lo precise. Estos servicios pueden estar activos o inactivos, pero siempre están disponibles para cuando se necesite usarlos. Estos servicios no se pueden activar ni desactivar, pero se pueden enmascarar.

**Generated:** Servicio que ha sido iniciado a través de un initscript SysV o LSB con systemd generator.

## 8. Els arxius de configuració de les Units ( siguin del tipus que siguin ) en quines 3 carpetes poden estar repartides?



## 9. Com podem afegir ( o sobreescriure ) opcions de configuració concretes, sense tenir que tocar les configuracions genèriques de la unit?

```
systemctl isolate graphical.target
```

## 10. Algunes "units" contenen el símbol @ en el seu nom (per exemple, nom@cadena.service); Que significa?



## 11. Les següents comandes serveixen per gestionar Units. Descriu el que fa cada una d'elles:



## 13. Com reiniciem una Unit?. Posa un exemple per exemple de una Unit de tipus socket

Una vegada creat si volem provar si funciona primer caldrà recarregar els serveis:

```
systemctl daemon-reload
```

## 14. Com sabem si un unit està activado, desactivado o ha fallat?

Podem veure totes les unitats disponibles amb:

```plaintext
systemctl list-unit-files
```

I les que s'estan executant:

```plaintext
systemctl list-units
```

O les que han fallat en iniciar-se:

```plaintext
systemctl --failed
```

## 15. Quina comanda fem servir si volem instalLar el servidor web apache2 (httpd)?

```
[Install]
WantedBy=multi-user.target
```

## 16. Què són els targets en SystemD?

Antigament hi havia els *runlevels* del 0 al 6 al SysV que en passar a SystemD són els *Targets*. Aquests fitxers porten com extensió *.target*.

La funció dels targets no és una altra que agrupar una sèrie de *service units* amb les seves dependències. Per exemple existeix el *graphical.target* que pot arrencar serveis com el *gdm.service*, *accounts-daemon.service*, etc... i ho farà en ordre i amb les sesves dependències.

## 17. Qué és un servei?. Com es crea un servei amb SystemD?. Quina estructura té? A on es creen?

Els fitxers de configuració de serveis de systemd tenen extensió *.service* i es troben en tres llocs:

- /usr/lib/systemd/system: Aquí hi ha els que venen amb els paquets que intal.lem (.deb, .rpm,...)

- /run/systemd/system: Aquí hi ha els que es creen en inicial el sistema. Són més prioritaris que els anterior.

- /etc/systemd/system: Aquí és on com administrador podem afegir-hi les nostres configuracions de serveis, que seran més prioritaris que tots els anteriors.

- Podem crear un fitxer a /etc/systemd/system/ anomenat webserver.service

  ```plaintext
  [Unit]
  Description=El meu servidor web
  
  [Service]
  ExecStart=/usr/bin/python3 -m http.server
  
  [Install]
  WantedBy=multi-user.target
  ```

  Una vegada creat si volem provar si funciona primer caldrà recarregar els serveis:

  ```plaintext
  systemctl daemon-reload
  ```

## 18. Una vegada creat un servei, que hem de fer perque el systemctl el tingui en compte?

Una vegada creat si volem provar si funciona primer caldrà recarregar els serveis:

systemctl daemon-reload

## 19. Com podem comprovar informació relacionada amb la execucio dels serveis? 

Podeu veure els logs del servei amb *journalctl -u webserver*.

## 20. Sota systemD, qui és l'encarregat de recolectar i emmagatzemar l'activitat del que va passant al sistema?

El Systemctl recolecta i emmagatzema.
